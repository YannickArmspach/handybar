HANDYBAR - WordPress Plugin
==================

A very little plugin to display the Wordpress Admin Bar at the top of the scrolling page (not fixed) for the admin view.

![HANDYBAR-ADMIN](preview.png)

And a small overflow widget for the site view.

![HANDYBAR-SITE](preview1.png)

currently in dev.
